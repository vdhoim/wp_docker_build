#!/bin/sh

now="$(date +'%d_%m_%Y_%H_%M_%S')"
filename="db_backup_$now".sql
backupfolder="/home/db_backup"
logsfolder="$backupfolder/logs"
logfile="$logsfolder/backup_log.log"

if [ ! -d "$backupfolder" ]; then
  mkdir "$backupfolder"
fi
if [ ! -d "$logsfolder" ]; then
  mkdir "$backupfolder/logs"
  touch "$logfile"
fi

dbuser="wordpress"
dbpassword=$(cat /wordpress-db-pw.txt)
dbname="wordpress"
dbhost="localhost"

echo "mysqldump started at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
mysqldump --user=$dbuser --password=$dbpassword --default-character-set=utf8 --host=$dbhost $dbname > "$backupfolder/$filename"
echo "mysqldump finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"

#delete old files (more than 30 days old)
find "$backupfolder" -name db_backup_* -mtime +30 -exec rm {} \;
echo "old files deleted" >> "$logfile"
echo "operation finished at $(date +'%d-%m-%Y %H:%M:%S')" >> "$logfile"
echo "*****************" >> "$logfile"
exit 0